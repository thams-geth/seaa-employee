package com.tts.seaa.employee.ui.task

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.response.task.TaskActivityResultItem
import com.tts.seaa.employee.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_activity_list.view.*

class TaskActivityAdapter(
    private val list: List<TaskActivityResultItem>,
    private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) :
    RecyclerView.Adapter<TaskActivityAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_activity_list, parent, false))
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtActivity.text = list[position].actTaskComment
        holder.itemView.txtTime.text = list[position].actDateAdded!!.substring(10, 16)

        holder.itemView.viewTop.visibility = View.VISIBLE
        holder.itemView.viewTop.visibility = View.VISIBLE

        if (list.size == 1) {
            holder.itemView.viewTop.visibility = View.GONE
            holder.itemView.viewBottom.visibility = View.GONE
        } else if (position == 0 && list.size > 1) {
            holder.itemView.viewTop.visibility = View.GONE
        } else if (position == list.size - 1) {
            holder.itemView.viewBottom.visibility = View.GONE
        }

    }


    override fun getItemCount(): Int {
        return list.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }


}