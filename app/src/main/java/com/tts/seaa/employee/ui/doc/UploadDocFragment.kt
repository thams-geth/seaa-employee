package com.tts.seaa.employee.ui.doc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.employee.R

class UploadDocFragment : Fragment() {

    companion object {
        fun newInstance() = UploadDocFragment()
    }

    private lateinit var viewModel: UploadDocViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_upload_doc, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(UploadDocViewModel::class.java)
        // TODO: Use the ViewModel
    }

}