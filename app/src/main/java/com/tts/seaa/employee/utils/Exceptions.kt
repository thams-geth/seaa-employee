package com.tts.seaa.employee.utils

import java.io.IOException

class ApiException(message: String) : IOException(message)
class NoConnectivityException: IOException()
class NoInternetException(message: String) : IOException(message)
class LocationPermissionNotGrantedException: Exception()
class DateNotFoundException: Exception()