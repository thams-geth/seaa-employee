package com.tts.seaa.employee.ui.home

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.api.RetrofitClient
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.data.response.DateItems
import com.tts.seaa.employee.data.response.task.TaskByDateResultItem
import com.tts.seaa.employee.databinding.FragmentHomeBinding
import com.tts.seaa.employee.ui.FragmentClickListener
import com.tts.seaa.employee.utils.changeTodayDateHeader
import com.tts.seaa.employee.utils.getTodayDate
import com.tts.seaa.employee.utils.getTodayDateHeader
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment(), DateListAdapter.OnItemClick {


    lateinit var fragmentClickListener: FragmentClickListener

    companion object {
        const val TAG = "HomeFragment"
        fun newInstance() = HomeFragment()

    }

    private lateinit var con: Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context
        fragmentClickListener = con as FragmentClickListener

    }

    private lateinit var binding: FragmentHomeBinding

    lateinit var homeRepository: HomeRepository
    lateinit var preferenceProvider: PreferenceProvider
    lateinit var taskList: ArrayList<TaskByDateResultItem>
    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        binding.txtCurrentDate.text = getTodayDateHeader()


        val api: ApiInterface = RetrofitClient(requireActivity()).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(requireActivity())
        viewModel = getViewModel()

        taskList = ArrayList()
        val taskByDate = getTodayDate()
        viewModel.getTaskByDate("2020-09-14")

        setDateListAdapter()
        // 2020-09-14

//        val currentTime: Date = Calendar.getInstance().getTime()


        viewModel.resultTaskByDate.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<TaskByDateResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }
            binding.txtTotalTask.text = "You have total ${list.size} task today"
            val homeAdapter = TaskListAdapter(list, con, fragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
    }

    private fun setDateListAdapter() {
        val c = Calendar.getInstance()

        val sdf = SimpleDateFormat("yyyy-MM-dd-EEE", Locale.getDefault())
        var dt: String? = null


        val list = ArrayList<DateItems>()
        for (i in 0..6) {
            dt = sdf.format(c.time)

            if (i == 0)
                list.add(DateItems(dt.substring(0, 4), dt.substring(5, 7), dt.substring(8, 10), dt.substring(11), true))
            else
                list.add(DateItems(dt.substring(0, 4), dt.substring(5, 7), dt.substring(8, 10), dt.substring(11), false))

            c.add(Calendar.DATE, 1) // number of days to add

        }
        print(list.toString())
        val dateListAdapter = DateListAdapter(list, con, this as DateListAdapter.OnItemClick)
        binding.rvDate.layoutManager = LinearLayoutManager(con, LinearLayoutManager.HORIZONTAL, false)
        binding.rvDate.adapter = dateListAdapter

    }


    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, context) as T
            }
        })[HomeViewModel::class.java]

    }

    override fun onClick(position: Int) {
        binding.txtCurrentDate.text = changeTodayDateHeader(position)
    }

}