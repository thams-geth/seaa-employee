package com.tts.seaa.employee.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.tts.seaa.employee.R
import com.tts.seaa.employee.ui.FragmentClickListener
import com.tts.seaa.employee.ui.alert.AlertFragment
import com.tts.seaa.employee.ui.doc.DocFragment
import com.tts.seaa.employee.ui.doc.UploadDocFragment
import com.tts.seaa.employee.ui.task.AddActivityFragment
import com.tts.seaa.employee.ui.task.TaskActivityFragment
import com.tts.seaa.employee.ui.task.TaskFragment
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity(), FragmentClickListener {

    var currentFragmentID = 1
    var clickedFragmentID = 1

    //    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()

        navigationView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.drawer_home -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()
                }
                R.id.drawer_task -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, TaskFragment.newInstance(), "Task").commit()
                }
                R.id.drawer_documents -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, DocFragment.newInstance(), "Doc").commit()
                }
                R.id.drawer_companyProfile -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
                R.id.drawer_feedback -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
                R.id.drawer_logout -> {
//                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
                }
            }
            drawer.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }



        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, HomeFragment.newInstance(), "Home").commit()
//                    navController.navigate(R.id.homeFragment)
                }
                R.id.task -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, TaskFragment.newInstance(), "Task").commit()
//                    navController.navigate(R.id.taskFragment)
                }
                R.id.doc -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, DocFragment.newInstance(), "Doc").commit()
//                    navController.navigate(R.id.docFragment)
                }
                R.id.alert -> {
                    supportFragmentManager.beginTransaction().replace(R.id.frameLayout, AlertFragment.newInstance(), "Alert").commit()
//                    navController.navigate(R.id.alertFragment)
                }
            }
            return@setOnNavigationItemSelectedListener true

        }

        //        val options = navOptions {
//           popUpTo =
//        }

//        navController = Navigation.findNavController(this, R.id.nav_host_fragment)

//        setupWithNavController(bottomNavigationView, navController)


//        navController.navigate(R.id.homeFragment)


//        bottomNavigationView.setupWithNavController(navController)
    }

    override fun onClick(fragment: String) {


        if (fragment == "UploadDoc") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, UploadDocFragment.newInstance(), "UploadDoc")
                .addToBackStack("Doc").commit()
//            Toast.makeText(this, "Doc", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClickWithTaskId(fragment: String, task_id: String) {
        if (fragment == "TaskActivity") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, TaskActivityFragment.newInstance(task_id), "TaskActivity")
                .addToBackStack("Task").commit()

        }

        if (fragment == "AddActivity") {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, AddActivityFragment.newInstance(task_id), "AddActivity")
                .addToBackStack("Task").commit()
        }
    }

    override fun onBack() {
        supportFragmentManager.popBackStack();

    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun drawerClick() {
        drawer.openDrawer(GravityCompat.START)
    }

//        bottomNavigationView.setupWithNavController(navController = navController)
    /*bottomNavigationView.setOnNavigationItemSelectedListener {

        when (it.itemId) {
            R.id.home -> {
                clickedFragmentID = 1
                updateFragment(currentFragmentID, clickedFragmentID)
                currentFragmentID = 1
            }
            R.id.task -> {
                clickedFragmentID = 2
                updateFragment(currentFragmentID, clickedFragmentID)
                currentFragmentID = 2

            }
            R.id.doc -> {
                clickedFragmentID = 3
                updateFragment(currentFragmentID, clickedFragmentID)
                currentFragmentID = 3
            }
            R.id.alert -> {
                clickedFragmentID = 4
                updateFragment(currentFragmentID, clickedFragmentID)
                currentFragmentID = 4
            }

        }
        return@setOnNavigationItemSelectedListener true
    }
}*/

/*
    private fun updateFragment(currentFragmentID: Int, clickedFragmentID: Int) {

        when (clickedFragmentID) {
            1 -> {
                when (currentFragmentID) {
                    1 -> {
                    }
                    2 -> {
                        navController.navigate(TaskFragmentDirections.actionTaskFragmentToHomeFragment())

                    }
                    3 -> {
                        navController.navigate(DocFragmentDirections.actionDocFragmentToHomeFragment())

                    }
                    4 -> {
                        navController.navigate(AlertFragmentDirections.actionAlertFragmentToHomeFragment())

                    }
                }
            }
            2 -> {
                when (currentFragmentID) {
                    1 -> {
                        navController.navigate(HomeFragmentDirections.actionHomeFragmentToTaskFragment())

                    }
                    2 -> {
                    }
                    3 -> {
                        navController.navigate(DocFragmentDirections.actionDocFragmentToTaskFragment())

                    }
                    4 -> {
                        navController.navigate(AlertFragmentDirections.actionAlertFragmentToTaskFragment())

                    }
                }
            }
            3 -> {
                when (currentFragmentID) {
                    1 -> {
                        navController.navigate(HomeFragmentDirections.actionHomeFragmentToDocFragment())

                    }
                    2 -> {
                        navController.navigate(TaskFragmentDirections.actionTaskFragmentToDocFragment())

                    }
                    3 -> {
                    }
                    4 -> {
                        navController.navigate(AlertFragmentDirections.actionAlertFragmentToDocFragment())

                    }
                }
            }
            4 -> {
                when (currentFragmentID) {
                    1 -> {
                        navController.navigate(HomeFragmentDirections.actionHomeFragmentToAlertFragment())

                    }
                    2 -> {
                        navController.navigate(TaskFragmentDirections.actionTaskFragmentToAddActivityFragment())

                    }
                    3 -> {
                        navController.navigate(DocFragmentDirections.actionDocFragmentToAlertFragment())

                    }
                    4 -> {

                    }
                }
            }
        }

    }
*/
/*
    override fun onBackPressed() {
        super.onBackPressed()
        Log.d("Back stack : ", "" + supportFragmentManager.backStackEntryCount)
        Log.d("currentDestination : ", "" + navController.currentDestination?.getId())

        if (navController.currentDestination?.id == 2131296590) { // For Task Fragment
            currentFragmentID = 2
            clickedFragmentID = 2
        } else if (navController.currentDestination?.id == 2131296402) { // For Doc Fragment
            currentFragmentID = 3
            clickedFragmentID = 3
        } else {
            currentFragmentID = 1
            clickedFragmentID = 1
            bottomNavigationView.selectedItemId = R.id.home

        }

//        if (supportFragmentManager.backStackEntryCount == 0) {
//            bottomNavigationView.selectedItemId = R.id.home
//        }

    }*/
}
