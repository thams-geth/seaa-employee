package com.tts.seaa.employee.data.response.login

import com.google.gson.annotations.SerializedName

data class FirmKeyResponse(

    val result: LoginResult? = null,
    val key: String? = null,
    val message: String? = null
)

data class LoginResult(

    @field:SerializedName("refKey")
    val refKey: String? = null
)
