package com.tts.seaa.employee.ui.task

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.api.RetrofitClient
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.databinding.FragmentAddActivityBinding
import com.tts.seaa.employee.ui.FragmentClickListener
import com.tts.seaa.employee.ui.home.HomeRepository
import com.tts.seaa.employee.ui.home.HomeViewModel
import com.tts.seaa.employee.utils.getTodayDateHeader
import kotlinx.android.synthetic.main.fragment_add_activity.*

private const val ARG_PARAM1 = "task_id"

class AddActivityFragment : Fragment() {


    lateinit var fragmentClickListener: FragmentClickListener
    private var taskId: String? = null
    private lateinit var con: Context
    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentAddActivityBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentClickListener = activity as FragmentClickListener
        arguments?.let { taskId = it.getString(ARG_PARAM1) }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_activity, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.txtCurrentDate.text = getTodayDateHeader()

        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()

        binding.btnSubmit.setOnClickListener {
            if (validation()) {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("emp_id", preferenceProvider.getEmpId())
                jsonObject.addProperty("task_id", taskId)
                jsonObject.addProperty("task_comment", binding.extComment.text.toString())

                when {
                    spnStatus.selectedItem.toString() == "OnGoing" -> {
                        jsonObject.addProperty("act_status", "1")
                    }
                    spnStatus.selectedItem.toString() == "Pending" -> {
                        jsonObject.addProperty("act_status", "2")
                    }
                    spnStatus.selectedItem.toString() == "Completed" -> {
                        jsonObject.addProperty("act_status", "3")
                    }
                }

                viewModel.addActivity(jsonObject)
            }

        }

        viewModel.resultAddActivity.observe(viewLifecycleOwner, Observer {
            if (it.message == "success") {
                Toast.makeText(con, "Task Added", Toast.LENGTH_SHORT).show()
                fragmentClickListener.onBack()

            } else
                Toast.makeText(con, "Try Again", Toast.LENGTH_SHORT).show()

        })

    }

    private fun validation(): Boolean {
        var valid = true
        when {
            binding.extComment.text.toString() == "" -> {
                valid = false
                binding.extComment.error = "Required"
            }
        }
        return valid
    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]


    }

    companion object {
        fun newInstance(task_id: String) = AddActivityFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, task_id)
            }
        }
    }

}