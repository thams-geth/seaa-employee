package com.tts.seaa.employee.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.response.DateItems
import kotlinx.android.synthetic.main.item_date.view.*

class DateListAdapter(private val list: List<DateItems>, private val context: Context, private val onItemClick: OnItemClick) : RecyclerView.Adapter<DateListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_date, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtDate.text = list[position].date
        holder.itemView.txtDay.text = list[position].day

        holder.itemView.setOnClickListener {
            for (pos in list.indices) {
                list[pos].isSelected = false
            }
            list[position].isSelected = list[position].isSelected != true
            onItemClick.onClick(position)
            notifyDataSetChanged()

        }

        if (list[position].isSelected) {
            holder.itemView.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.txtDate.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            holder.itemView.txtDay.setTextColor(ContextCompat.getColor(context, android.R.color.tab_indicator_text))
        } else {
            holder.itemView.card.setCardBackgroundColor(ContextCompat.getColor(context, R.color.blueDark))
            holder.itemView.txtDate.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.itemView.txtDay.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

    public interface OnItemClick {
        fun onClick(position: Int)
    }
}