package com.tts.seaa.employee.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.ui.home.HomeActivity
import com.tts.seaa.employee.ui.login.FirmActivity


class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val preferenceProvider: PreferenceProvider = PreferenceProvider(this)
        Handler().postDelayed(Runnable {
            if (preferenceProvider.getMobileNo() != null) {
                val i = Intent(this, HomeActivity::class.java)
                startActivity(i)
                finish()
            } else {
                val i = Intent(this, FirmActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 500)
    }
}