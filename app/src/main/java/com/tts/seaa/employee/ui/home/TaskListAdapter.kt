package com.tts.seaa.employee.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.response.task.TaskByDateResultItem
import com.tts.seaa.employee.ui.FragmentClickListener
import kotlinx.android.synthetic.main.item_task_list.view.*

class TaskListAdapter(
    private val list: List<TaskByDateResultItem>, private val context: Context,
    private val fragmentClickListener: FragmentClickListener
) : RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task_list, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.txtTaskName.text = list[position].taskTitle
        holder.itemView.txtEmployeeName.text = list[position].empName
        holder.itemView.txtStatus.text = list[position].status

        holder.itemView.setOnClickListener {
            fragmentClickListener.onClickWithTaskId("TaskActivity", list[position].taskId!!)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}