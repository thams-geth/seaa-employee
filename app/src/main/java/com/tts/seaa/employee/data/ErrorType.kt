package com.tts.seaa.employee.data

import okhttp3.ResponseBody
import org.json.JSONObject

enum class ErrorType {
    NETWORK, // IO
    TIMEOUT, // Socket
    UNKNOWN //Anything else
}


fun getErrorMessage(responseBody: ResponseBody?): String {
    return try {
        val jsonObject = JSONObject(responseBody!!.string())
        when {
            jsonObject.has("message") -> jsonObject.getString("message")
            jsonObject.has("error") -> jsonObject.getString("error")
            else -> "Something wrong happened"
        }
    } catch (e: Exception) {
        "Something wrong happened"
    }
}


