package com.tts.seaa.employee.ui.login

import com.google.gson.JsonObject
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.response.login.FirmKeyResponse
import com.tts.seaa.employee.data.response.login.LoginResponse
import retrofit2.Response

class LoginRepo(private val api: ApiInterface) {

    suspend fun getFirmKey(jsonObject: JsonObject): Response<FirmKeyResponse> {
        return api.getFirmKey(jsonObject)
    }

    suspend fun sendMobileNo(jsonObject: JsonObject): Response<LoginResponse> {
        return api.sendMobileNo(jsonObject)
    }

    suspend fun verifyOTP(jsonObject: JsonObject): Response<LoginResponse> {
        return api.verifyOTP(jsonObject)
    }
}