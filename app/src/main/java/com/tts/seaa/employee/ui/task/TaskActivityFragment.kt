package com.tts.seaa.employee.ui.task

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.api.RetrofitClient
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.data.response.task.TaskActivityResultItem
import com.tts.seaa.employee.databinding.FragmentTaskActivityBinding
import com.tts.seaa.employee.ui.FragmentClickListener
import com.tts.seaa.employee.ui.home.HomeRepository
import com.tts.seaa.employee.ui.home.HomeViewModel

private const val ARG_PARAM1 = "task_id"

class TaskActivityFragment : Fragment() {

    lateinit var fragmentClickListener: FragmentClickListener
    private var taskId: String? = null
    private lateinit var con: Context
    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentTaskActivityBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentClickListener = activity as FragmentClickListener
        arguments?.let { taskId = it.getString(ARG_PARAM1) }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task_activity, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()

        binding.btnAddActivity.visibility = View.GONE

        taskId?.let {
            viewModel.getTaskActivity(it)
            binding.btnAddActivity.visibility = View.VISIBLE
        }

        viewModel.resultTaskActivity.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<TaskActivityResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }
            val homeAdapter = TaskActivityAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

        binding.btnAddActivity.setOnClickListener {

            fragmentClickListener.onClickWithTaskId("AddActivity", taskId!!)

        }

    }


    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]


    }

    companion object {
        fun newInstance(task_id: String) = TaskActivityFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_PARAM1, task_id)
            }
        }
    }
}