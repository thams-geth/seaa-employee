package com.tts.seaa.employee.data.response.task

import com.google.gson.annotations.SerializedName

data class TaskByDateResponse(

    val result: List<TaskByDateResultItem?>? = null,
    val key: String? = null,
    val message: String? = null

)

data class TaskByDateResultItem(

    @field:SerializedName("is_active")
    val isActive: String? = null,

    @field:SerializedName("due_date")
    val dueDate: String? = null,

    @field:SerializedName("emp_name")
    val empName: String? = null,

    @field:SerializedName("task_title")
    val taskTitle: String? = null,

    @field:SerializedName("task_id")
    val taskId: String? = null,

    @field:SerializedName("priority")
    val priority: String? = null,

    @field:SerializedName("ta_employee")
    val taEmployee: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
