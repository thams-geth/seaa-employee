package com.tts.seaa.employee.data.response.task

import com.google.gson.annotations.SerializedName

data class TaskActivityResponse(

    val result: List<TaskActivityResultItem?>? = null,
    val key: String? = null,
    val message: String? = null

)

data class TaskActivityResultItem(

    @field:SerializedName("act_task_comment")
    val actTaskComment: String? = null,

    @field:SerializedName("act_id")
    val actId: String? = null,

    @field:SerializedName("act_task_id")
    val actTaskId: String? = null,

    @field:SerializedName("act_date_added")
    val actDateAdded: String? = null,

    @field:SerializedName("firm_id")
    val firmId: String? = null,

    @field:SerializedName("act_task_status")
    val actTaskStatus: String? = null,

    @field:SerializedName("act_emp_id")
    val actEmpId: String? = null
)
