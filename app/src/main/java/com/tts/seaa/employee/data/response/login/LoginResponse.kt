package com.tts.seaa.employee.data.response.login

data class LoginResponse(

    val message: String? = null,
    val key: String? = null,
    val emp_id: String? = null
)
