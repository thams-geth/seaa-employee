package com.tts.seaa.employee.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.api.RetrofitClient
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    lateinit var preferenceProvider: PreferenceProvider
    lateinit var loginRepo: LoginRepo
    lateinit var api: ApiInterface
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        api = RetrofitClient(this).getClient()
        loginRepo = LoginRepo(api)
        preferenceProvider = PreferenceProvider(this)

        loginViewModel = getViewModel()

        constLogin.visibility = View.VISIBLE
        constOTP.visibility = View.GONE


        imgSendOTP.setOnClickListener {
            if (extMobileNumber.text.toString() != "" && extMobileNumber.text.toString().length == 10) {
                loginViewModel.sendMobileNo(extMobileNumber.text.toString())
            } else {
                extMobileNumber.error = "Please Enter Mobile No"
            }
        }

        imgVerifyOTP.setOnClickListener {
            if (extOTP1.text.toString() != "" && extOTP2.text.toString() != "" && extOTP3.text.toString() != "" && extOTP4.text.toString() != "") {
                val otp = extOTP1.text.toString() + extOTP2.text.toString() + extOTP3.text.toString() + extOTP4.text.toString()
                loginViewModel.verifyOTP(otp)

            }

        }

        loginViewModel.mobileNoLiveData.observe(this, Observer {
            if (it.message == "success") {
                constLogin.visibility = View.GONE
                constOTP.visibility = View.VISIBLE
            }
        })

        loginViewModel.OTPLiveData.observe(this, Observer {
            if (it.message == "success") {
                preferenceProvider.saveMobileNo(extMobileNumber.text.toString())

                val intent = Intent(this, HomeActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }

        })

        extOTP1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) extOTP2.requestFocus()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        extOTP2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) extOTP3.requestFocus() else if (s.toString()
                        .isEmpty()
                ) extOTP1.requestFocus()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        extOTP3.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) extOTP4.requestFocus() else if (s.toString()
                        .isEmpty()
                ) extOTP2.requestFocus()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        extOTP4.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length == 1) hideKeyBoard() else if (s.toString()
                        .isEmpty()
                ) extOTP3.requestFocus()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    private fun hideKeyBoard() {
        val imm = ContextCompat.getSystemService(extOTP4.context, InputMethodManager::class.java)
        imm?.hideSoftInputFromWindow(extOTP4.windowToken, 0)

    }

    private fun getViewModel(): LoginViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return LoginViewModel(
                    loginRepo,
                    preferenceProvider,
                    this@LoginActivity
                ) as T
            }
        })[LoginViewModel::class.java]


    }
}