package com.tts.seaa.employee.data.api

import android.renderscript.Allocation
import com.google.gson.JsonObject
import com.tts.seaa.employee.data.response.login.FirmKeyResponse
import com.tts.seaa.employee.data.response.login.LoginResponse
import com.tts.seaa.employee.data.response.task.AllTaskResponse
import com.tts.seaa.employee.data.response.task.TaskActivityResponse
import com.tts.seaa.employee.data.response.task.TaskByDateResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiInterface {
    //login
//    @FormUrlEncoded
//    @POST
//    fun login(@Field("userid") userid: String, @Field("password") password: String): Call<LoginResponse>

    @POST("seaa-api/mob/fe/empauth/getfirmKey")
    suspend fun getFirmKey(@Body body: JsonObject): Response<FirmKeyResponse>

    @POST("seaa-api/mob/fe/empauth/emplogin")
    suspend fun sendMobileNo(@Body body: JsonObject): Response<LoginResponse>

    @POST("seaa-api/mob/fe/empauth/otpauth")
    suspend fun verifyOTP(@Body body: JsonObject): Response<LoginResponse>


    @POST("seaa-api/mob/fe/emptask/getAllTasks")
    suspend fun getAllTasks(@Body firm_id: JsonObject): Response<AllTaskResponse>

    @POST("seaa-api/mob/fe/emptask/getTasksbydate")
    suspend fun getTaskByDate(@Body firm_id: JsonObject): Response<TaskByDateResponse>

    @POST("seaa-api/mob/fe/emptask/getTaskActivities")
    suspend fun getTaskActivity(@Body firm_id: JsonObject): Response<TaskActivityResponse>

    @POST("seaa-api/mob/fe/emptask/addActivity")
    suspend fun addActivity(@Body addTask: JsonObject): Response<LoginResponse>


    //empty


    //empty


    //empty

    @Headers("Content-Type: application/json")
    @POST("authenticate")
//    suspend fun login(@Body jsonObject: JsonObject): Call<LoginResponse>


    @GET("casedetails/agreementDetailsPerf")
    suspend fun allRes(@Query("branchID") branchID: Int): Response<Allocation>


    @GET("casedetails/agreementDetailsPerf")
    suspend fun allocation(@Query("branchID") branchID: Int): Call<ResponseBody>

//    @GET("contactrecording/questions")
//    suspend fun questions(): Response<List<Questions>>

//    @GET("api/v1/employees")
//    suspend fun questions(): Callback<ResponseBody>


}