package com.tts.seaa.employee.ui.home

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.JsonObject
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.data.response.login.LoginResponse
import com.tts.seaa.employee.data.response.task.AllTaskResponse
import com.tts.seaa.employee.data.response.task.TaskActivityResponse
import com.tts.seaa.employee.data.response.task.TaskByDateResponse
import com.tts.seaa.employee.utils.Coroutines

class HomeViewModel(
    private val homeRepository: HomeRepository,
    private val preferenceProvider: PreferenceProvider,
    private val context: Context?

) : ViewModel() {

    private var _resultAllTask: MutableLiveData<AllTaskResponse> = MutableLiveData<AllTaskResponse>()
    private var _resultTaskByDate: MutableLiveData<TaskByDateResponse> = MutableLiveData<TaskByDateResponse>()
    private var _resultTaskActivity: MutableLiveData<TaskActivityResponse> = MutableLiveData<TaskActivityResponse>()
    private var _resultAddActivity: MutableLiveData<LoginResponse> = MutableLiveData<LoginResponse>()


    var resultAllTask: LiveData<AllTaskResponse> = _resultAllTask
    var resultTaskByDate: LiveData<TaskByDateResponse> = _resultTaskByDate
    var resultTaskActivity: LiveData<TaskActivityResponse> = _resultTaskActivity
    var resultAddActivity: LiveData<LoginResponse> = _resultAddActivity


    fun getAllTask() {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("emp_id", preferenceProvider.getEmpId())

                val tasks = homeRepository.getAllTasks(jsonObject)
                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultAllTask.value = tasks.body()!!
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTaskByDate(task_date: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("emp_id", preferenceProvider.getEmpId())
                jsonObject.addProperty("task_date", task_date)

                val tasks = homeRepository.getTaskByDate(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTaskByDate.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getTaskActivity(task_id: String) {
        Coroutines.main {
            try {
                val jsonObject = JsonObject()
                jsonObject.addProperty("firm_key", preferenceProvider.getFirmKey())
                jsonObject.addProperty("task_id", task_id)

                val tasks = homeRepository.getTaskActivity(jsonObject)

                if (tasks.isSuccessful && tasks.body() != null) {
                    if (tasks.body()!!.result != null) {
                        _resultTaskActivity.value = tasks.body()
                    } else
                        Toast.makeText(context, tasks.body()!!.message, Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun addActivity(addTask: JsonObject) {
        Coroutines.main {
            try {
                val tasks = homeRepository.addActivity(addTask)
                if (tasks.isSuccessful && tasks.body() != null) {
                    _resultAddActivity.value = tasks.body()!!
                }
            } catch (e: Exception) {
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()

            }
        }
    }

}