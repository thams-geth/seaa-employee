package com.tts.seaa.employee.data.response

data class DateItems(

    var year: String,
    var month: String,
    var date: String,
    var day: String,
    var isSelected: Boolean
)
