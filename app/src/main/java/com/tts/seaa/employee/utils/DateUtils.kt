package com.tts.seaa.employee.utils

import java.text.SimpleDateFormat
import java.util.*

fun getTodayDateHeader(): String {
    val sdf = SimpleDateFormat("EEEE , MMM dd", Locale.getDefault())
    return sdf.format(Date())
}

fun changeTodayDateHeader(position: Int): String {
    val c = Calendar.getInstance()
    c.add(Calendar.DATE, position) // number of days to add

    val sdf = SimpleDateFormat("EEEE , MMM dd", Locale.getDefault())
    return sdf.format(c.time)
}

fun getTodayDate(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    return sdf.format(Date())
}