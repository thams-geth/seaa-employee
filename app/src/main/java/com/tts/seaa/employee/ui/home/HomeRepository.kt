package com.tts.seaa.employee.ui.home

import com.google.gson.JsonObject
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.response.login.LoginResponse
import com.tts.seaa.employee.data.response.task.AllTaskResponse
import com.tts.seaa.employee.data.response.task.TaskActivityResponse
import com.tts.seaa.employee.data.response.task.TaskByDateResponse
import retrofit2.Response

class HomeRepository(private val api: ApiInterface) {

//    suspend fun getEmployee(jsonObject: JsonObject): Response<EmployeeResponse> {
//        return api.getEmployees(jsonObject)
//    }
//
//    suspend fun getEmployeeActivity(jsonObject: JsonObject): Response<EmployeeActivityResponse> {
//        return api.getEmployeeActivities(jsonObject)
//    }

    suspend fun getAllTasks(jsonObject: JsonObject): Response<AllTaskResponse> {
        return api.getAllTasks(jsonObject)
    }

    suspend fun getTaskByDate(jsonObject: JsonObject): Response<TaskByDateResponse> {
        return api.getTaskByDate(jsonObject)
    }

    suspend fun getTaskActivity(jsonObject: JsonObject): Response<TaskActivityResponse> {
        return api.getTaskActivity(jsonObject)
    }

    suspend fun addActivity(jsonObject: JsonObject): Response<LoginResponse> {
        return api.addActivity(jsonObject)
    }
//
//    suspend fun getTeams(jsonObject: JsonObject): Response<TeamResponse> {
//        return api.getTeams(jsonObject)
//    }
//
//    suspend fun getCompanies(jsonObject: JsonObject): Response<CompanyResponse> {
//        return api.getCompanies(jsonObject)
//    }
//
//    suspend fun getTemplate(jsonObject: JsonObject): Response<TemplateResponse> {
//        return api.getTemplate(jsonObject)
//    }
//
//    suspend fun addTask(jsonObject: JsonObject): Response<AddTaskResponse> {
//        return api.addTask(jsonObject)
//    }


}