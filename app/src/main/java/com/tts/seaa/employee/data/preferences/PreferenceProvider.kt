package com.tts.seaa.employee.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.google.gson.JsonObject


private const val FIRM_KEY = "firm_key"
private const val EMP_ID = "emp_id"
private const val EMP_MOBILE = "emp_mobile"


class PreferenceProvider(context: Context) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun saveFirmKey(token: String) {
        preference.edit().putString(FIRM_KEY, token).apply()
    }

    fun getFirmKey(): String? {
        return preference.getString(FIRM_KEY, null)
    }

    fun saveEmpId(emp_id: String) {
        preference.edit().putString(EMP_ID, emp_id).apply()
    }

    fun getEmpId(): String? {
        return preference.getString(EMP_ID, null)
    }

    fun saveMobileNo(emp_mobile: String) {
        preference.edit().putString(EMP_MOBILE, emp_mobile).apply()
    }

    fun getMobileNo(): String? {
        return preference.getString(EMP_MOBILE, null)
    }

    fun getFirmKeyAsJsonObject(): JsonObject {
        val jsonObject = JsonObject()
        jsonObject.addProperty("firm_key", getFirmKey())
        return jsonObject
    }


}