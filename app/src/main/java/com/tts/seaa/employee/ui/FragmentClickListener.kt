package com.tts.seaa.employee.ui

interface FragmentClickListener {
    fun onClick(fragment: String)
    fun onClickWithTaskId(fragment: String, task_id: String)
    fun onBack()
    fun drawerClick()


}