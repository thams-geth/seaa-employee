package com.tts.seaa.employee.ui.task

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tts.seaa.employee.R
import com.tts.seaa.employee.data.api.ApiInterface
import com.tts.seaa.employee.data.api.RetrofitClient
import com.tts.seaa.employee.data.preferences.PreferenceProvider
import com.tts.seaa.employee.data.response.task.TaskByDateResultItem
import com.tts.seaa.employee.databinding.FragmentTaskBinding
import com.tts.seaa.employee.ui.FragmentClickListener
import com.tts.seaa.employee.ui.home.HomeRepository
import com.tts.seaa.employee.ui.home.HomeViewModel
import com.tts.seaa.employee.ui.home.TaskListAdapter
import com.tts.seaa.employee.utils.getTodayDateHeader


class TaskFragment : Fragment() {

    lateinit var fragmentClickListener: FragmentClickListener
    private lateinit var con: Context
    lateinit var preferenceProvider: PreferenceProvider
    lateinit var homeRepository: HomeRepository
    lateinit var api: ApiInterface
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: FragmentTaskBinding


    override fun onAttach(context: Context) {
        super.onAttach(context)
        con = context

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentClickListener = activity as FragmentClickListener
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        binding.txtCurrentDate.text = getTodayDateHeader()


        api = RetrofitClient(con).getClient()
        homeRepository = HomeRepository(api)
        preferenceProvider = PreferenceProvider(con)

        viewModel = getViewModel()
        viewModel.getAllTask()

        viewModel.resultAllTask.observe(viewLifecycleOwner, Observer {

            var list = ArrayList<TaskByDateResultItem>()
            if (it != null) {
                for (i in it.result!!) {
                    list.add(i!!)
                }
            }
            val homeAdapter = TaskListAdapter(list, con, con as FragmentClickListener)
            binding.recyclerView.layoutManager = LinearLayoutManager(con)
            binding.recyclerView.adapter = homeAdapter

        })

        binding.drawerIcon.setOnClickListener {
            fragmentClickListener.drawerClick()
        }
    }

    private fun getViewModel(): HomeViewModel {
        return ViewModelProvider(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return HomeViewModel(homeRepository, preferenceProvider, con) as T
            }
        })[HomeViewModel::class.java]


    }

    companion object {
        fun newInstance() = TaskFragment()
    }

}